<html>
<head>
	<title>Estructuras de control en php</title>
</head>
<body>

<?php
/* Las estructuras de control en lenguajes de programacion se utilizan para realizar operaciones por ejemplo: selectivas, repetitivas, y ambas;
*if (selectiva)
*for (repetitivas)
*while (repetitivas-condicional)
*/
// estructura de control IF, ejemplo:
$x=0;
$y=10;
$z=5;
if($x == 0)
			{  echo "La variable es cero"; }
else
	{echo "La variable no es cero";}

// Estructura for, ejemplo
$colores = array('blue','green','yellow','violet','white','black','orange','red','blue','green','yellow','violet','white','black','orange','red');

echo "<br />Los numeros naturales hasta el 10<br />";
for ($i=0; $i <=10 ; $i++) 
{ 
	echo " $i";
}
for ($i=0 	; $i <8 ; $i++) { 
	echo "<br />Elemento $i = [" . $colores[$i] . "]";
}

// funcion count(), sirve para contar los elementos de un vector
for ($i=0 	; $i < count($colores) ; $i++) { 
	echo "<br /><h2>Color<h1 style='color:$colores[$i]'>$i : $colores[$i] </h1></h2>";
}

//ejercicio : crear un vector de 20 elementos con numeros del 1 al 20, y mostrar solo los multiplos de 2

$vector  = array('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17' ,'18','19','20');
for ($i=0; $i <count($vector) ; $i++)
 { 
	if($vector[$i]%2==0)
		{echo "<br /><h1 style='color:$colores[$i]'> $vector[$i]</h1>"; }
}

?>
</body>
</html>